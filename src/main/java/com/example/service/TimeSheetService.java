package com.example.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entities.Schedular;
import com.example.entities.Timesheet;
import com.example.repositories.SchedularRepository;
import com.example.repositories.TimeSheetRepository;

@Service
public class TimeSheetService {

	@Autowired
	private TimeSheetRepository timeSheetRepository;
	
	@Autowired
	private SchedularRepository schedularRepository;
	
	public void updateTimeSheetSchedular() {
		
		List<Schedular> listScheduler = new ArrayList<>();
		listScheduler = (List<Schedular>)schedularRepository.findAll();

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, 2);
		Date today = cal.getTime();
		for (Schedular sche : listScheduler) {
			Timesheet timesheet = new Timesheet();
			timesheet.setDate(today);
			timesheet.setNumbercase(3);
			timesheet.setBooked(0);
			timesheet.setShedularID(sche);
			timeSheetRepository.save(timesheet);
		}
		
	}
	
	public List<Timesheet> getTimeSheetByDate(Date date) {
		return timeSheetRepository.findByDate(date);
	}
	
}
