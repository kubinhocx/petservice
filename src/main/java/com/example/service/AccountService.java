package com.example.service;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entities.Account;
import com.example.repositories.AccountRepository;
import com.twilio.Twilio;
import com.twilio.http.TwilioRestClient;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@Service
public class AccountService {
	
	public static final String ACCOUNT_SID = "AC8afc42512fe2182da96de0dab96060a0";
	 public static final String AUTH_TOKEN = "6f37e1da9f2b6f9217c039e79a300d6d";
	 
	@Autowired
	private AccountRepository accountRepo;

	public Account findAllPhoneNumber(String phonenum) {
		Account account = accountRepo.findByPhonenum(phonenum);
		return account;
	}
	
	public String sendSmsMessage(String phonenum) {
		String newPhonenum = "+84" + phonenum.substring(1);
		Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
		
		//Random 6 number 
		Random rnd = new Random();
	    int number = rnd.nextInt(999999);
	    String random = String.format("%06d", number);
		
		Message message = Message.creator(new PhoneNumber(newPhonenum), new PhoneNumber("+12173393689"), "Hello user. This is your verification number: " + random).create();
		return random;
	}
}
