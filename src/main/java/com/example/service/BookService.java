package com.example.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entities.Book;
import com.example.entities.Timesheet;
import com.example.repositories.BookRepository;
import com.example.repositories.TimeSheetRepository;

@Service
public class BookService {

	@Autowired
	private BookRepository bookRepository;
	
	@Autowired
	private TimeSheetRepository timeSheetRepository;
	
	
	public Book bookAService(Book book) {
		Book booking = new Book();
		book.setAccountID(book.getAccountID());
		book.setServiceID(book.getServiceID());
		
		Timesheet timesheet = new Timesheet();
		timesheet.setId(book.getTimeSheetID().getId());
		timesheet.setDate(book.getTimeSheetID().getDate());
		timesheet.setNumbercase(book.getTimeSheetID().getNumbercase());
		timesheet.setBooked(book.getTimeSheetID().getBooked() + 1);
		timesheet.setShedularID(book.getTimeSheetID().getShedularID());
		timeSheetRepository.save(timesheet);
		
		book.setTimeSheetID(timesheet);
		book.setStatus(book.getStatus());
		bookRepository.save(booking);

		return booking;
	}
}
