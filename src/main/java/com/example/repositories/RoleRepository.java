package com.example.repositories;

import org.springframework.data.repository.CrudRepository;

import com.example.entities.Role;

public interface RoleRepository extends CrudRepository<Role, Integer>{

}
