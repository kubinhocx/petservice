package com.example.repositories;

import org.springframework.data.repository.CrudRepository;

import com.example.entities.Schedular;

public interface SchedularRepository extends CrudRepository<Schedular, Integer>{

}
