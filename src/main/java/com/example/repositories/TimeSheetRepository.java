package com.example.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.example.entities.Timesheet;

public interface TimeSheetRepository extends CrudRepository<Timesheet, Integer>{
	
	List<Timesheet> findByDate(Date date);
	
	@Query(value = "Select * from Time_sheet where ID = :id",
            nativeQuery=true
    )
    Timesheet getByID(@Param("id")int id);
	
}
