package com.example.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import com.example.entities.Review;

public interface ReviewRepository extends CrudRepository<Review, Integer>{
	 @Query(value = "SELECT * FROM Review order by ID DESC",
	            nativeQuery=true
	    )
	    public List<Review> findAllAndSort();
	 
	 @Query(value = "SELECT sum(Point) FROM Review",
	            nativeQuery=true
	    )
	    public String sumPoint();
	 }
