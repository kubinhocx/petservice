package com.example.repositories;

import org.springframework.data.repository.CrudRepository;

import com.example.entities.Service;

public interface ServiceRepository extends CrudRepository<Service, Integer>{

}
