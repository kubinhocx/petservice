package com.example.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.example.entities.Account;

public interface AccountRepository extends CrudRepository<Account, Integer>{

	Account findByPhonenum(String phonenum);
	
	@Query(value = "Select * from Account where Phonenum = :phonenum",
			nativeQuery=true
				)
	Account getByPhone(@Param("phonenum") String phonenum);
}
