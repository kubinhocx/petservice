package com.example.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.entities.Book;

@Repository
public interface BookRepository extends CrudRepository<Book, Integer>{
//	@Async
	 @Query(value = "Update Book set Status = :status where ID = :id",
	            nativeQuery=true
	    )
	    void updateStatus(@Param("status")String status,@Param("id") int id);

	 @Query(value = "Select * from Book where ID = :id",
	            nativeQuery=true
	    )
	    Book getByID(@Param("id") int id);
	 
	 @Query(value = "Select * from Book order by ID DESC",
	            nativeQuery=true
	    )
	    List<Book> getAllByDESC();
	 @Query(value = "Select * from Book where AccountID = :accountid order by ID DESC",
	            nativeQuery=true
	    )
	    List<Book> getByAccount(@Param("accountid")int accountid);
}
