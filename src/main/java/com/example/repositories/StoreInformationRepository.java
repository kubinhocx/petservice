package com.example.repositories;

import org.springframework.data.repository.CrudRepository;

import com.example.entities.StoreInformation;

public interface StoreInformationRepository extends CrudRepository<StoreInformation, Integer>{

}
