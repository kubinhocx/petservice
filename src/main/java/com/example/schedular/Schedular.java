package com.example.schedular;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.example.service.TimeSheetService;

@Component
public class Schedular {
	private static final Logger LOGGER = LoggerFactory.getLogger(Schedular.class);
	@Autowired
	private TimeSheetService timeSheetService;

	@Scheduled(cron = "0 0 0 * * *")
	public void autoTriggerTimeSheet() {
        
        timeSheetService.updateTimeSheetSchedular();
        LOGGER.info("Auto schedular API");
    }
}
