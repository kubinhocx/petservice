/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Chung Nguyen
 */
@Entity
@Table(name = "Store_Information")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StoreInformation.findAll", query = "SELECT s FROM StoreInformation s")
    , @NamedQuery(name = "StoreInformation.findById", query = "SELECT s FROM StoreInformation s WHERE s.id = :id")
    , @NamedQuery(name = "StoreInformation.findByName", query = "SELECT s FROM StoreInformation s WHERE s.name = :name")
    , @NamedQuery(name = "StoreInformation.findByAddress", query = "SELECT s FROM StoreInformation s WHERE s.address = :address")
    , @NamedQuery(name = "StoreInformation.findByLocationX", query = "SELECT s FROM StoreInformation s WHERE s.locationX = :locationX")
    , @NamedQuery(name = "StoreInformation.findByLoactionY", query = "SELECT s FROM StoreInformation s WHERE s.loactionY = :loactionY")
    , @NamedQuery(name = "StoreInformation.findByPhonenum", query = "SELECT s FROM StoreInformation s WHERE s.phonenum = :phonenum")})
public class StoreInformation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Name", nullable = false, length = 50)
    private String name;
    @Basic(optional = false)
    @Column(name = "Address", nullable = false, length = 500)
    private String address;
    @Basic(optional = false)
    @Column(name = "Location_X", nullable = false)
    private double locationX;
    @Basic(optional = false)
    @Column(name = "Loaction_Y", nullable = false)
    private double loactionY;
    @Basic(optional = false)
    @Column(name = "Phonenum", nullable = false, length = 12)
    private String phonenum;

    public StoreInformation() {
    }

    public StoreInformation(Integer id) {
        this.id = id;
    }

    public StoreInformation(Integer id, String name, String address, double locationX, double loactionY, String phonenum) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.locationX = locationX;
        this.loactionY = loactionY;
        this.phonenum = phonenum;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLocationX() {
        return locationX;
    }

    public void setLocationX(double locationX) {
        this.locationX = locationX;
    }

    public double getLoactionY() {
        return loactionY;
    }

    public void setLoactionY(double loactionY) {
        this.loactionY = loactionY;
    }

    public String getPhonenum() {
        return phonenum;
    }

    public void setPhonenum(String phonenum) {
        this.phonenum = phonenum;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StoreInformation)) {
            return false;
        }
        StoreInformation other = (StoreInformation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.example.entities.StoreInformation[ id=" + id + " ]";
    }
    
}
