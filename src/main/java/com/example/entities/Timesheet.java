/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author Chung Nguyen
 */
@Entity
@Table(name = "Time_sheet")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Timesheet.findAll", query = "SELECT t FROM Timesheet t")
    , @NamedQuery(name = "Timesheet.findById", query = "SELECT t FROM Timesheet t WHERE t.id = :id")
    , @NamedQuery(name = "Timesheet.findByDate", query = "SELECT t FROM Timesheet t WHERE t.date = :date")
    , @NamedQuery(name = "Timesheet.findByNumbercase", query = "SELECT t FROM Timesheet t WHERE t.numbercase = :numbercase")
    , @NamedQuery(name = "Timesheet.findByBooked", query = "SELECT t FROM Timesheet t WHERE t.booked = :booked")})
public class Timesheet implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date date;
    @Basic(optional = false)
    @Column(name = "Number_case", nullable = false)
    private int numbercase;
    @Basic(optional = false)
    @Column(name = "Booked", nullable = false)
    private int booked;
    @JoinColumn(name = "Shedular_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Schedular shedularID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "timeSheetID")
    @JsonIgnore
    private Collection<Book> bookCollection;
    public Timesheet() {
    }
    public Timesheet(Integer id) {
        this.id = id;
    }

    public Timesheet(Integer id, Date date, int numbercase, int booked) {
        this.id = id;
        this.date = date;
        this.numbercase = numbercase;
        this.booked = booked;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getNumbercase() {
        return numbercase;
    }

    public void setNumbercase(int numbercase) {
        this.numbercase = numbercase;
    }

    public int getBooked() {
        return booked;
    }

    public void setBooked(int booked) {
        this.booked = booked;
    }

    @XmlTransient
    public Collection<Book> getBookCollection() {
        return bookCollection;
    }

    public void setBookCollection(Collection<Book> bookCollection) {
        this.bookCollection = bookCollection;
    }

    public Schedular getShedularID() {
        return shedularID;
    }

    public void setShedularID(Schedular shedularID) {
        this.shedularID = shedularID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Timesheet)) {
            return false;
        }
        Timesheet other = (Timesheet) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.example.entities.Timesheet[ id=" + id + " ]";
    }
    
}
