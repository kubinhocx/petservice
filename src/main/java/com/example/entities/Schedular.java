/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author Chung Nguyen
 */
@Entity
@Table(name = "Schedular")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Schedular.findAll", query = "SELECT s FROM Schedular s")
    , @NamedQuery(name = "Schedular.findById", query = "SELECT s FROM Schedular s WHERE s.id = :id")
    , @NamedQuery(name = "Schedular.findByStarttime", query = "SELECT s FROM Schedular s WHERE s.starttime = :starttime")
    , @NamedQuery(name = "Schedular.findByEndtime", query = "SELECT s FROM Schedular s WHERE s.endtime = :endtime")})
public class Schedular implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Start_time", nullable = false)
    @Temporal(TemporalType.TIME)
    private Date starttime;
    @Basic(optional = false)
    @Column(name = "End_time", nullable = false)
    @Temporal(TemporalType.TIME)
    private Date endtime;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "shedularID")
    @JsonIgnore
    private Collection<Timesheet> timesheetCollection;

    public Schedular() {
    }

    public Schedular(Integer id) {
        this.id = id;
    }

    public Schedular(Integer id, Date starttime, Date endtime) {
        this.id = id;
        this.starttime = starttime;
        this.endtime = endtime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStarttime() {
        return starttime;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    @XmlTransient
    public Collection<Timesheet> getTimesheetCollection() {
        return timesheetCollection;
    }

    public void setTimesheetCollection(Collection<Timesheet> timesheetCollection) {
        this.timesheetCollection = timesheetCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Schedular)) {
            return false;
        }
        Schedular other = (Schedular) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.example.entities.Schedular[ id=" + id + " ]";
    }
    
}
