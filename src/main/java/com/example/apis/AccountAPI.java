package com.example.apis;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.entities.Account;
import com.example.entities.Role;
import com.example.repositories.AccountRepository;
import com.example.service.AccountService;

@Controller
public class AccountAPI {
	@Autowired
	AccountRepository accountRepository;
	@Autowired
	AccountService accountService;
	private final String  path = "/account";
	@ResponseBody
	@GetMapping(path + "/getAll")
	public List<Account> getAllAccount(){
		return (List<Account>) accountRepository.findAll();
	}
	
	@ResponseBody
	@GetMapping(path+ "/findByID/{id}")
	public Optional<Account> findByID(@PathVariable("id") int id){
		return accountRepository.findById(id);
	}
	
	@ResponseBody
	@GetMapping(path + "/count")
	public int countAccount() {
		return (int) accountRepository.count();
	}
	
	@ResponseBody
	@DeleteMapping(path + "/delete/{id}")
	public void deleteAccount(@PathVariable("id") int id) {
		accountRepository.deleteById(id);
	}
	
	@ResponseBody
	@PostMapping(path + "/create/{account}")
	public void createAccount(@PathVariable("account") Account account) {
		accountRepository.save(account);
	}
	
	@ResponseBody
	@PutMapping(path + "/update/{account}")
	public void updateAccount(@PathVariable("account") Account account) {
		accountRepository.save(account);
	}
	
	@ResponseBody
	@GetMapping(path + "/findByPhonenum/{phonenum}")
	public Account findByPhonenum(@PathVariable("phonenum") String phonenum) {
		Account account = accountService.findAllPhoneNumber(phonenum);
		if (account == null) {
			String password = accountService.sendSmsMessage(phonenum);
			account = new Account();
			account.setPhonenum(phonenum);
			account.setPassword(password);
			account.setVerify(false);
			account.setRoleID(new Role(2));
			accountRepository.save(account);
			
		} else if (account.getVerify() == false) {
			String password = accountService.sendSmsMessage(phonenum);
			account.setPassword(password);
			accountRepository.save(account);
		} 
		return account;
	}
	
	@ResponseBody
	@PostMapping(path + "/createAccount")
	public void createAccountMobile(@RequestBody Account account) {
		
		accountRepository.save(account);
		
	}
	
	@ResponseBody
	@PostMapping(path + "/update")
	public void updateInfo(@RequestBody Account account) {
		Account account2 = accountRepository.getByPhone(account.getPhonenum());
		account2.setFullname(account.getFullname());
		accountRepository.save(account2);
	}
}
