package com.example.apis;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.entities.Book;
import com.example.entities.Timesheet;
import com.example.repositories.BookRepository;
import com.example.repositories.TimeSheetRepository;

@Controller
public class BookAPI {

	@Autowired
	BookRepository bookRepository;
	
	@Autowired
	private TimeSheetRepository timeSheetRepository;
	
	private final String  path = "/book";
	@ResponseBody
	@GetMapping(path + "/getAll")
	public List<Book> getAllBook(){
		return (List<Book>) bookRepository.findAll();
	}
	
	@ResponseBody
	@GetMapping(path+ "/findByID/{id}")
	public Optional<Book> findByID(@PathVariable("id") int id){
		return bookRepository.findById(id);
	}
	
	@ResponseBody
	@GetMapping(path + "/count")
	public int countBook() {
		return (int) bookRepository.count();
	}
	
	@ResponseBody
	@DeleteMapping(path + "/delete/{id}")
	public void deleteBook(@PathVariable("id") int id) {
		bookRepository.deleteById(id);
	}
	
	@ResponseBody
	@PostMapping(path + "/create")
	public void createBook(@RequestBody Book Book) {
		bookRepository.save(Book);
	}
	
	@ResponseBody
	@PutMapping(path + "/update/{Book}")
	public void updateBook(@PathVariable("accooount") Book Book) {
		bookRepository.save(Book);
	}
	
	@ResponseBody
	@PostMapping(path + "/createBooking")
	public void createBooking(@RequestBody Book book) {
		Optional<Timesheet> timesheet = timeSheetRepository.findById(book.getTimeSheetID().getId());
		timesheet.get().setBooked(timesheet.get().getBooked() + 1);
		timeSheetRepository.save(timesheet.get());
		
		bookRepository.save(book);
	}
	@ResponseBody
	@PutMapping(path + "/updateStatus")
	public void updateStatus(@RequestBody Book book) {
		bookRepository.updateStatus(book.getStatus(),book.getId());
	}
	@ResponseBody
	@PostMapping(path + "/updateStatus2")
	public void updateStatus2(@RequestBody Book book) {
		Book book2 = bookRepository.getByID(book.getId());
		book2.setStatus(book.getStatus());
		
		bookRepository.save(book2);
	}
	@ResponseBody
	@GetMapping(path+ "/getByAccount/{id}")
	public List<Book> getByAccount(@PathVariable("id") int accountid){
		return bookRepository.getByAccount(accountid);
	}
	

}
