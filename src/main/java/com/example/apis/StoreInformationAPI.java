package com.example.apis;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.entities.StoreInformation;
import com.example.repositories.StoreInformationRepository;

@Controller
public class StoreInformationAPI {

	@Autowired
	StoreInformationRepository storeInformationRepository;
	private final String  path = "/storeInformation";
	@ResponseBody
	@GetMapping(path + "/getAll")
	public List<StoreInformation> getAllStoreInformation(){
		return (List<StoreInformation>) storeInformationRepository.findAll();
	}
	
	@ResponseBody
	@GetMapping(path+ "/findByID/{id}")
	public Optional<StoreInformation> findByID(@PathVariable("id") int id){
		return storeInformationRepository.findById(id);
	}
	
	@ResponseBody
	@GetMapping(path + "/count")
	public int countStoreInformation() {
		return (int) storeInformationRepository.count();
	}
	
	@ResponseBody
	@DeleteMapping(path + "/delete/{id}")
	public void deleteStoreInformation(@PathVariable("id") int id) {
		storeInformationRepository.deleteById(id);
	}
	
	@ResponseBody
	@PostMapping(path + "/create/{StoreInformation}")
	public void createStoreInformation(@PathVariable("StoreInformation") StoreInformation StoreInformation) {
		storeInformationRepository.save(StoreInformation);
	}
	
	@ResponseBody
	@PutMapping(path + "/update/{StoreInformation}")
	public void updateStoreInformation(@PathVariable("accooount") StoreInformation StoreInformation) {
		storeInformationRepository.save(StoreInformation);
	}
}