package com.example.apis;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.entities.Book;
import com.example.entities.Timesheet;
import com.example.repositories.BookRepository;
import com.example.repositories.TimeSheetRepository;
import com.example.service.BookService;
import com.example.service.TimeSheetService;

@Controller
public class TimeSheetAPI {

	@Autowired
	TimeSheetRepository timeSheetRepository;
	
	@Autowired
	TimeSheetService service;
	
	@Autowired
	BookRepository bookRepository;
	
	private final String  path = "/timesheet";
	@ResponseBody
	@GetMapping(path + "/getAll")
	public List<Timesheet> getAllTimesheet(){
		return (List<Timesheet>) timeSheetRepository.findAll();
	}
	
	@ResponseBody
	@GetMapping(path+ "/findByID/{id}")
	public Optional<Timesheet> findByID(@PathVariable("id") int id){
		return timeSheetRepository.findById(id);
	}
	
	@ResponseBody
	@GetMapping(path + "/count")
	public int countTimesheet() {
		return (int) timeSheetRepository.count();
	}
	
	@ResponseBody
	@DeleteMapping(path + "/delete/{id}")
	public void deleteTimesheet(@PathVariable("id") int id) {
		timeSheetRepository.deleteById(id);
	}
	
	@ResponseBody
	@PostMapping(path + "/create/{Timesheet}")
	public void createTimesheet(@PathVariable("Timesheet") Timesheet Timesheet) {
		timeSheetRepository.save(Timesheet);
	}
	
	@ResponseBody
	@PutMapping(path + "/update/{Timesheet}")
	public void updateTimesheet(@PathVariable("accooount") Timesheet Timesheet) {
		timeSheetRepository.save(Timesheet);
	}
	
	@ResponseBody
	@GetMapping(path + "/getTimesheet/{date}")
	public List<Timesheet> getListTimeSheetByDate(@PathVariable("date") String date) throws ParseException{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date d = sdf.parse(date);
		
		return service.getTimeSheetByDate(d);
	
	}
	
	@ResponseBody
	@GetMapping(path + "/updateTimesheet")
	public void updateTimeSheet() {
		service.updateTimeSheetSchedular();
	}
	
	@ResponseBody
	@PostMapping(path+"/updateDeleteBook/{id}")
	public void updateDeleteBook(@PathVariable("id") int id) {
		Book book = bookRepository.getByID(id);
		Timesheet timesheet = timeSheetRepository.getByID(book.getTimeSheetID().getId());
		timesheet.setBooked(timesheet.getBooked() - 1);
		timeSheetRepository.save(timesheet);
		
	}
}
