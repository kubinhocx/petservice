package com.example.apis;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.entities.Schedular;
import com.example.repositories.SchedularRepository;

@Controller
public class SchedularAPI {

	@Autowired
	SchedularRepository schedularRepository;
	private final String  path = "/schedular";
	@ResponseBody
	@GetMapping(path + "/getAll")
	public List<Schedular> getAllSchedular(){
		return (List<Schedular>) schedularRepository.findAll();
	}
	
	@ResponseBody
	@GetMapping(path+ "/findByID/{id}")
	public Optional<Schedular> findByID(@PathVariable("id") int id){
		return schedularRepository.findById(id);
	}
	
	@ResponseBody
	@GetMapping(path + "/count")
	public int countSchedular() {
		return (int) schedularRepository.count();
	}
	
	@ResponseBody
	@DeleteMapping(path + "/delete/{id}")
	public void deleteSchedular(@PathVariable("id") int id) {
		schedularRepository.deleteById(id);
	}
	
	@ResponseBody
	@PostMapping(path + "/create/{Schedular}")
	public void createSchedular(@PathVariable("Schedular") Schedular Schedular) {
		schedularRepository.save(Schedular);
	}
	
	@ResponseBody
	@PutMapping(path + "/update/{Schedular}")
	public void updateSchedular(@PathVariable("accooount") Schedular Schedular) {
		schedularRepository.save(Schedular);
	}
}
