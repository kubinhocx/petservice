package com.example.apis;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.entities.Account;
import com.example.entities.Review;
import com.example.entities.Role;
import com.example.repositories.ReviewRepository;
@Controller
public class ReviewAPI {
	@Autowired
	ReviewRepository reviewRepository;
	private final String  path = "/review";
	@ResponseBody
	@GetMapping(path + "/getAll")
	public List<Review> getAllReview(){
		return (List<Review>) reviewRepository.findAll();
	}
	
	@ResponseBody
	@GetMapping(path+ "/findByID/{id}")
	public Optional<Review> findByID(@PathVariable("id") int id){
		return reviewRepository.findById(id);
	}
	
	@ResponseBody
	@GetMapping(path + "/count")
	public int countReview() {
		return (int) reviewRepository.count();
	}
	
	@ResponseBody
	@DeleteMapping(path + "/delete/{id}")
	public void deleteReview(@PathVariable("id") int id) {
		reviewRepository.deleteById(id);
	}
	
	@ResponseBody
	@PostMapping(path + "/create")
	public void createReview(@RequestBody Review Review) {
		reviewRepository.save(Review);
	}
	
	@ResponseBody
	@PutMapping(path + "/update/{Review}")
	public void updateReview(@PathVariable("Review") Review Review) {
		reviewRepository.save(Review);
	}
	
	@ResponseBody
	@PostMapping(path + "/createReview")
	public void createReviewMobile(@RequestBody Review Review) {
		reviewRepository.save(Review);
	}
	@ResponseBody
	@GetMapping(path + "/getAllSort")
	public List<Review> getByAccount(){
		return  reviewRepository.findAllAndSort();
	}
	@ResponseBody
	@GetMapping(path + "/getSumPoint")
	public String getSumPoint(){
		return  (String) reviewRepository.sumPoint();
	}
}
