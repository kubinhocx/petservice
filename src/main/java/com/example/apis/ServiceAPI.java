package com.example.apis;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.entities.Service;
import com.example.repositories.ServiceRepository;

@Controller
public class ServiceAPI {
	@Autowired
	ServiceRepository serviceRepository;
	private final String  path = "/service";
	@ResponseBody
	@GetMapping(path + "/getAll")
	public List<Service> getAllService(){
		return (List<Service>) serviceRepository.findAll();
	}
	
	@ResponseBody
	@GetMapping(path+ "/findByID/{id}")
	public Optional<Service> findByID(@PathVariable("id") int id){
		return serviceRepository.findById(id);
	}
	
	@ResponseBody
	@GetMapping(path + "/count")
	public int countService() {
		return (int) serviceRepository.count();
	}
	
	@ResponseBody
	@DeleteMapping(path + "/delete/{id}")
	public void deleteService(@PathVariable("id") int id) {
		serviceRepository.deleteById(id);
	}
	
	@ResponseBody
	@PostMapping(path + "/create/{Service}")
	public void createService(@PathVariable("Service") Service Service) {
		serviceRepository.save(Service);
	}
	
	@ResponseBody
	@PutMapping(path + "/update/{Service}")
	public void updateService(@PathVariable("accooount") Service Service) {
		serviceRepository.save(Service);
	}
}