package com.example.apis;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.entities.Account;
import com.example.entities.Role;
import com.example.repositories.RoleRepository;

@Controller
public class RoleAPI {
	private final String path = "/role";
	@Autowired
	RoleRepository roleRepository;
	
	@ResponseBody
	@GetMapping(path +"/getAll")
	public List<Role> getAllRole(){
		return (List<Role>) roleRepository.findAll();
	}
	@ResponseBody
	@GetMapping(path+ "/findByID/{id}")
	public Optional<Role> findByID(@PathVariable("id") int id){
		return roleRepository.findById(id);
	}
	
	@ResponseBody
	@GetMapping(path + "/count")
	public int countRole() {
		return (int) roleRepository.count();
	}
	
	@ResponseBody
	@DeleteMapping(path + "/delete/{id}")
	public void deleteAccount(@PathVariable("id") int id) {
		roleRepository.deleteById(id);
	}
	
	@ResponseBody
	@PostMapping(path + "/create/{roleDTO}")
	public void createAccount(@PathVariable("roleDTO") Role roleDTO) {
		roleRepository.save(roleDTO);
	}
	
	@ResponseBody
	@PutMapping(path + "/update/{roleDTO}")
	public void updateAccount(@PathVariable("roleDTO") Role roleDTO) {
		roleRepository.save(roleDTO);
	}
}
